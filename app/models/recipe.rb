class Recipe < ActiveRecord::Base
  has_many :ingredients
  accepts_nested_attributes_for :ingredients, :allow_destroy => true, reject_if: proc { |attributes| attributes['name'].blank? }

  validates_presence_of :name
end
